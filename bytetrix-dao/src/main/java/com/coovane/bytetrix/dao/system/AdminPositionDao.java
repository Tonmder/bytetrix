package com.coovane.bytetrix.dao.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coovane.bytetrix.domain.system.AdminPosition;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 描述：岗位数据库操作dao层
 * </p >
 *
 * @author: liuchangrong
 * @date: 2022/11/27 20:11
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Mapper
public interface AdminPositionDao extends BaseMapper<AdminPosition> {
}
