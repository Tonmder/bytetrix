package com.coovane.bytetrix.dao.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coovane.bytetrix.domain.system.AdminUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *     管理员用户dao接口
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/14 22:11
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Mapper
public interface AdminUserDao extends BaseMapper<AdminUser> {
}
