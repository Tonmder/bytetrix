package com.coovane.bytetrix.dao.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coovane.bytetrix.domain.system.AdminResource;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *     菜单资源管理dao
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 21:04
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Mapper
public interface AdminResourceDao extends BaseMapper<AdminResource> {
}
