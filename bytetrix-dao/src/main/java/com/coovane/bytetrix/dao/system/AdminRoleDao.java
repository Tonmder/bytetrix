package com.coovane.bytetrix.dao.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coovane.bytetrix.domain.system.AdminRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *     管理员角色dao接口
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 21:00
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Mapper
public interface AdminRoleDao extends BaseMapper<AdminRole> {
}
