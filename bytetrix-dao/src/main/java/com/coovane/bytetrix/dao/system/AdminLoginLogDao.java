package com.coovane.bytetrix.dao.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coovane.bytetrix.domain.system.AdminLoginLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *     管理员用户登录日志
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 20:12
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Mapper
public interface AdminLoginLogDao extends BaseMapper<AdminLoginLog> {
}
