package com.coovane.bytetrix.framework.util;

import jakarta.servlet.http.HttpServletRequest;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Properties;

/**
 * <p>
 *     获取客户端IP地址工具类
 * </p>
 *
 * @author: Hsu
 * @date: 2023/3/30 21:43
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class IPUtils {

    /**
     * 获取客户端IP地址
     * @param request
     * @return
     */
    public static String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if(ip == null || ip.length() == 0 ||  "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 ||  "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if ((ip == null) || (ip.length() == 0) ||  ("unknown".equalsIgnoreCase(ip))) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if ((ip == null) || (ip.length() == 0) ||  ("unknown".equalsIgnoreCase(ip))) {
            ip = request.getHeader("CLIENT_IP");
        }
        if(ip == null || ip.length() == 0 ||  "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
            if(isLinuxOS()){
                getHostAddresses();
            }else {
                // windows环境下可通过这个方式获取到局域网ip
                // 根据网卡取本机配置的IP
                InetAddress inet=null;
                try {
                    inet = InetAddress.getLocalHost();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ip= inet.getHostAddress();
            }
        }
        // 多级代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if(ip != null && ip.length() > 15){
            if(ip.indexOf(",")>0){
                ip = ip.substring(0,ip.indexOf(","));
            }
        }
        return ip;
    }

    /**
     * 判断当前系统是否是Linux环境
     * @return
     */
    public static boolean isLinuxOS() {
        Properties prop = System.getProperties();
        String os = prop.getProperty("os.name");
        if (os != null &&  os.toLowerCase().indexOf("linux") > -1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取网卡地址，此方法待完善
     * @return
     */
    public static String getHostAddresses() {
        StringBuilder builder = new StringBuilder();
        Enumeration<NetworkInterface> netInterfaces =  null;
        try {
            netInterfaces =  NetworkInterface.getNetworkInterfaces();
            while (netInterfaces.hasMoreElements()) {
                NetworkInterface ni =  netInterfaces.nextElement();
                Enumeration<InetAddress> ips =  ni.getInetAddresses();
                while (ips.hasMoreElements()) {
                    String hostAddress =  ips.nextElement().getHostAddress();
                    builder.append(hostAddress).append(",");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

}
