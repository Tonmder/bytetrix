package com.coovane.bytetrix.domain.system;

import com.baomidou.mybatisplus.annotation.*;
import com.coovane.bytetrix.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 *     管理员角色实体
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 22:36
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "sys_admin_role")
public class AdminRole extends BaseEntity implements Serializable {
    @Serial
    private static final long serialVersionUID = -9128341066737471213L;
    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 角色代码
     */
    private String roleCode;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 数据范围：1-全部数据，2-本部门数据，3-本部门及以下数据，4-仅本人数据，5-自定数据
     */
    private Integer dataScope;
    /**
     * 是否启用：0-禁用，1-启用
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer enabled;
}
