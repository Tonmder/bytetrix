package com.coovane.bytetrix.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

/**
 * <p>
 *     实体对象基类
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/26 21:43
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
public class BaseEntity {
    /**
     * 是否删除：0-未删除，1-已删除
     */
    private Integer deleteFlag;
    /**
     * 创建时间，可通过Mybatis Plus实现自动填充
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 创建人ID
     */
    private String createdBy;
    /**
     * 更新时间，可通过Mybatis Plus实现自动填充
     */
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;
    /**
     * 更新人ID
     */
    private String updatedBy;
}
