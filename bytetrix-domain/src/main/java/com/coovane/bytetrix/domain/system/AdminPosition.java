package com.coovane.bytetrix.domain.system;

import com.baomidou.mybatisplus.annotation.*;
import com.coovane.bytetrix.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 * 描述：岗位实体类
 * </p >
 *
 * @author: liuchangrong
 * @date: 2022/11/26 20:58
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "sys_admin_position")
public class AdminPosition extends BaseEntity implements Serializable {
    @Serial
    private static final long serialVersionUID = 2652207284434798636L;
    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 岗位编码
     */
    private String positionCode;
    /**
     * 岗位名称
     */
    private String positionName;
    /**
     * 显示顺序(从小到大升序)
     */
    private Integer sortNum;
    /**
     * 岗位状态(0 停用 1 正常)
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer enabled;

}
