package com.coovane.bytetrix.domain.system;

import com.baomidou.mybatisplus.annotation.*;
import com.coovane.bytetrix.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 *     菜单资源实体
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 22:34
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "sys_admin_resource")
public class AdminResource extends BaseEntity implements Serializable {
    @Serial
    private static final long serialVersionUID = 7372897203015455080L;
    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 上级资源ID
     */
    private String parentId;
    /**
     * 资源名称
     */
    private String resourceName;
    /**
     * 资源类型：0-目录，1-菜单，2-按钮
     */
    private Integer resourceType;
    /**
     * 路由地址
     */
    private String routePath;
    /**
     * 访问URL
     */
    private String resourceUrl;
    /**
     * 授权是否显示：0-隐藏，1-显示
     */
    private Integer visible;
    /**
     * 权限标识
     */
    private String permission;
    /**
     * 资源图标
     */
    private String displayIcon;
    /**
     * 显示顺序(从小到大升序)
     */
    private Integer sortNum;
    /**
     * 是否启用：0-禁用，1-启用
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer enabled;

}
