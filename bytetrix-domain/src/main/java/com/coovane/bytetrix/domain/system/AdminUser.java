package com.coovane.bytetrix.domain.system;

import com.baomidou.mybatisplus.annotation.*;
import com.coovane.bytetrix.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 * 管理员用户实体
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/14 22:08
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "sys_admin_user")
public class AdminUser extends BaseEntity implements Serializable {
    @Serial
    private static final long serialVersionUID = 2652207284434798637L;
    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 加密盐值
     */
    private String salt;
    /**
     * 密码
     */
    private String password;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 电子邮箱
     */
    private String email;
    /**
     * 性别，2-保密，1-男，0-女
     */
    private Integer gender;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 是否启用：0-禁用，1-启用
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer enabled;
    /**
     * 部门ID
     */
    private String departmentId;
    /**
     * 岗位ID
     */
    private String positionId;

}
