package com.coovane.bytetrix.domain.system;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *     管理员用户登录日志实体
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 20:00
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
@TableName(value = "sys_admin_login_log")
public class AdminLoginLog implements Serializable {
    @Serial
    private static final long serialVersionUID = 5759560557237459029L;
    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 管理员用户ID
     */
    private String userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 登录IP
     */
    private String loginIp;
    /**
     * 归属地：广东广州
     */
    private String attribution;
    /**
     * 浏览器类型
     */
    private String browserType;
    /**
     * 平台类型
     */
    private Integer platform;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 登录异常提醒
     */
    private String warning;
}
