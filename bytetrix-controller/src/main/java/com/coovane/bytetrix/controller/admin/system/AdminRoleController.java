package com.coovane.bytetrix.controller.admin.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.coovane.bytetrix.common.response.GenericResult;
import com.coovane.bytetrix.common.model.system.AdminRoleDTO;
import com.coovane.bytetrix.common.model.system.AdminRoleListVO;
import com.coovane.bytetrix.common.model.system.AdminRoleQuery;
import com.coovane.bytetrix.service.system.AdminRoleService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 管理后台角色controller
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 21:59
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/admin/role")
public class AdminRoleController {

    private final AdminRoleService adminRoleService;

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    @GetMapping(value = "/v1/list")
    public GenericResult<IPage<AdminRoleListVO>> pageQuery(AdminRoleQuery query) {
        return GenericResult.success(adminRoleService.pageQuery(query));
    }

    /**
     * 新增
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "/v1/create")
    public GenericResult<String> create(@RequestBody @Valid AdminRoleDTO dto) {
        return GenericResult.success(adminRoleService.create(dto));
    }

    /**
     * 编辑更新
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "/v1/modify")
    public GenericResult<String> modify(@RequestBody @Valid AdminRoleDTO dto) {
        return GenericResult.success(adminRoleService.modify(dto));
    }

    /**
     * 获取单个详情
     *
     * @param id 主键ID
     * @return
     */
    @GetMapping(value = "/v1/getDetail")
    public GenericResult<AdminRoleDTO> getDetail(String id) {
        return GenericResult.success(adminRoleService.getDetail(id));
    }

    /**
     * 根据ID逻辑删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/v1/delete")
    public GenericResult<String> delete(String id) {
        return GenericResult.success(adminRoleService.deleteById(id));
    }

}
