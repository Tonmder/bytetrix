package com.coovane.bytetrix.controller.admin.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.coovane.bytetrix.common.model.system.ResetPasswordDTO;
import com.coovane.bytetrix.common.response.GenericResult;
import com.coovane.bytetrix.common.model.system.AdminUserDTO;
import com.coovane.bytetrix.common.model.system.AdminUserListVO;
import com.coovane.bytetrix.common.model.system.AdminUserQuery;
import com.coovane.bytetrix.service.system.AdminUserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 后台系统用户controller
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/8 22:16
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/admin/user")
public class AdminUserController {

    private final AdminUserService adminUserService;

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    @GetMapping(value = "/v1/list")
    public GenericResult<IPage<AdminUserListVO>> pageQuery(AdminUserQuery query) {
        return GenericResult.success(adminUserService.pageQuery(query));
    }

    /**
     * 新增
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "/v1/create")
    public GenericResult<String> create(@RequestBody @Valid AdminUserDTO dto) {
        return GenericResult.success(adminUserService.create(dto));
    }

    /**
     * 编辑更新
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "/v1/modify")
    public GenericResult<String> modify(AdminUserDTO dto) {
        return GenericResult.success(adminUserService.modify(dto));
    }

    /**
     * 获取单个详情
     *
     * @param id 主键ID
     * @return
     */
    @GetMapping(value = "/v1/getDetail")
    public GenericResult<AdminUserDTO> getDetail(String id) {
        return GenericResult.success(adminUserService.getDetail(id));
    }

    /**
     * 根据ID逻辑删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/v1/delete")
    public GenericResult<String> delete(String id) {
        return GenericResult.success(adminUserService.deleteById(id));
    }

    /**
     * 重置密码
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "/v1/resetPassword")
    public GenericResult<String> resetPassword(@RequestBody @Valid ResetPasswordDTO dto) {
        return GenericResult.success(adminUserService.resetPassword(dto));
    }

}
