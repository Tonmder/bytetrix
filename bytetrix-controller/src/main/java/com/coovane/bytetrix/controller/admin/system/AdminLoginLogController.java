package com.coovane.bytetrix.controller.admin.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.coovane.bytetrix.common.response.GenericResult;
import com.coovane.bytetrix.common.model.system.AdminLoginLogListVO;
import com.coovane.bytetrix.common.model.system.AdminLoginLogQuery;
import com.coovane.bytetrix.service.system.AdminLoginLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *     管理员用户登录日志controller
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 20:15
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/admin/loginLog")
public class AdminLoginLogController {

    private final AdminLoginLogService adminLoginLogService;

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    @GetMapping(value = "/v1/list")
    public GenericResult<IPage<AdminLoginLogListVO>> pageQuery(AdminLoginLogQuery query) {
        return GenericResult.success(adminLoginLogService.pageQuery(query));
    }

}
