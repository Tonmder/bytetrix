package com.coovane.bytetrix.controller.admin.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.coovane.bytetrix.common.response.GenericResult;
import com.coovane.bytetrix.common.model.system.AdminResourceDTO;
import com.coovane.bytetrix.common.model.system.AdminResourceListVO;
import com.coovane.bytetrix.common.model.system.AdminResourceQuery;
import com.coovane.bytetrix.service.system.AdminResourceService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 菜单资源管理controller
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 21:11
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/admin/resource")
public class AdminResourceController {

    private final AdminResourceService adminResourceService;

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    @GetMapping(value = "/v1/list")
    public GenericResult<IPage<AdminResourceListVO>> pageQuery(AdminResourceQuery query) {
        return GenericResult.success(adminResourceService.pageQuery(query));
    }

    /**
     * 新增
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "/v1/create")
    public GenericResult<String> create(@RequestBody @Valid AdminResourceDTO dto) {
        return GenericResult.success(adminResourceService.create(dto));
    }

    /**
     * 编辑更新
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "/v1/modify")
    public GenericResult<String> modify(@RequestBody @Valid AdminResourceDTO dto) {
        return GenericResult.success(adminResourceService.modify(dto));
    }

    /**
     * 获取单个详情
     *
     * @param id 主键ID
     * @return
     */
    @GetMapping(value = "/v1/getDetail")
    public GenericResult<AdminResourceDTO> getDetail(String id) {
        return GenericResult.success(adminResourceService.getDetail(id));
    }

    /**
     * 根据ID逻辑删除
     *
     * @param id
     * @return
     */
    @DeleteMapping(value = "/v1/delete")
    public GenericResult<String> delete(String id) {
        return GenericResult.success(adminResourceService.deleteById(id));
    }

}
