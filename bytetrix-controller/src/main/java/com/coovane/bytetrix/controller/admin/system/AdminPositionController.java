package com.coovane.bytetrix.controller.admin.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.coovane.bytetrix.common.response.GenericResult;
import com.coovane.bytetrix.common.model.system.AdminPositionDTO;
import com.coovane.bytetrix.common.model.system.AdminPositionListVO;
import com.coovane.bytetrix.common.model.system.AdminPositionQuery;
import com.coovane.bytetrix.service.system.AdminPositionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 描述：岗位管理Controller
 * </p >
 *
 * @author: liuchangrong
 * @date: 2022/11/27 21:15
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/admin/position")
public class AdminPositionController {

    private final AdminPositionService adminPositionService;

    /**
     * 分页查询
     * @param query
     * @return
     */
    @GetMapping(value = "/v1/list")
    public GenericResult<IPage<AdminPositionListVO>> pageQuery(AdminPositionQuery query) {
        return GenericResult.success(adminPositionService.pageQuery(query));
    }

    /**
     * 获取单个详情
     * @param id 主键ID
     * @return
     */
    @GetMapping(value = "/v1/getDetail")
    public GenericResult<AdminPositionDTO> getDetail(String id) {
        return GenericResult.success(adminPositionService.getDetail(id));
    }

    /**
     * 根据ID逻辑删除
     * @param id
     * @return
     */
    @DeleteMapping(value = "/v1/delete")
    public GenericResult<String> delete(String id) {
        return GenericResult.success(adminPositionService.deleteById(id));
    }

    /**
     * 新增
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "/v1/create")
    public GenericResult<String> create(@RequestBody AdminPositionDTO dto) {
        return GenericResult.success(adminPositionService.create(dto));
    }

    /**
     * 编辑更新
     *
     * @param dto
     * @return
     */
    @PostMapping(value = "/v1/modify")
    public GenericResult<String> modify(@RequestBody AdminPositionDTO dto) {
        return GenericResult.success(adminPositionService.modify(dto));
    }

}
