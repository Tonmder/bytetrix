package com.coovane.bytetrix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 *     程序启动主入口
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/9 22:57
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@SpringBootApplication
public class BytetrixBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(BytetrixBootApplication.class, args);
    }

}
