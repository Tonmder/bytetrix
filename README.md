<p align="center">
  <img alt="logo" src="https://qiniu.inkur.cn/stella/logo/ico-60x60.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">Bytetrix 0.0.1</h1>
<h4 align="center" style="text-align: center">基于 Vue/Arco Design 和 Spring Boot 3.X前后端分离的单体应用</h4>

## 平台简介

本系统目标是构建一个易于使用的项目管理系统

## 注意事项

- 本系统基于**JDK17**，部分包不向下兼容，系统导入后请检查Project structure，检查JDK版本
- 本系统虽然使用了Lombok，但是不是100%覆盖，部分实体需要个性化setter时，使用IDE快速生成getter/setter后自定义实现
- 需要使用反射时出现错误（Java 9及以上版本中引入的Java Platform Module System引起）则需要添加虚拟机启动参数--add-opens java.base/java.lang=ALL-UNNAMED
- 使用jasypt-spring-boot-starter进行配置文件脱敏时，可以通过命令行执行加密操作java -cp  D:\Maven\repository\org\jasypt\jasypt\1.9.3\jasypt-1.9.3.jar org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI input="TextYouNeedToEncrypt" password=YourPassword algorithm=PBEWITHHMACSHA512ANDAES_256 ivGeneratorClassName=org.jasypt.iv.RandomIvGenerator，其中input是需要加密的明文（需要注意数据库连接信息不得出现=，否则加密失败）
- jasypt-spring-boot-starter项目地址：https://github.com/ulisesbocchio/jasypt-spring-boot

## 工程结构
~~~
bytetrix    
  ├── bytetrix-boot                  // 启动类
  ├── bytetrix-common                // 公共依赖
  ├── bytetrix-controller            // 控制器层
  ├── bytetrix-dao                   // 数据库操作层
  ├── bytetrix-domain                // 领域对象层（数据库实体）
  ├── bytetrix-service               // 服务实现层
  ├── pom.xml                        // 公共依赖管理
~~~

## 在线体验

## 系统预览

## 特别鸣谢

