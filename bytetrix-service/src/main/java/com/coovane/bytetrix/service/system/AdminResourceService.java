package com.coovane.bytetrix.service.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.coovane.bytetrix.domain.system.AdminResource;
import com.coovane.bytetrix.common.model.system.AdminResourceDTO;
import com.coovane.bytetrix.common.model.system.AdminResourceListVO;
import com.coovane.bytetrix.common.model.system.AdminResourceQuery;

/**
 * <p>
 *     菜单资源管理serivce接口
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 21:07
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public interface AdminResourceService extends IService<AdminResource> {
    /**
     * 列表分页查询
     * @param query
     * @return
     */
    IPage<AdminResourceListVO> pageQuery(AdminResourceQuery query);
    /**
     * 新增
     * @param dto
     * @return
     */
    String create(AdminResourceDTO dto);
    /**
     * 编辑更新
     * @param dto
     * @return
     */
    String modify(AdminResourceDTO dto);
    /**
     * 获取单个详情
     * @param id
     * @return
     */
    AdminResourceDTO getDetail(String id);
    /**
     * 根据ID逻辑删除
     * @param id
     * @return
     */
    String deleteById(String id);

    /**
     * 校验是否存在重复数据
     * @param dto
     * @return
     */
    void assertIfDuplicate(AdminResourceDTO dto);

}
