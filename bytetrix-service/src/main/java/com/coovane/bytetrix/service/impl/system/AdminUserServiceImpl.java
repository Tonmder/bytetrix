package com.coovane.bytetrix.service.impl.system;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coovane.bytetrix.common.enumeration.ErrorCodeEnum;
import com.coovane.bytetrix.common.exception.GenericAssert;
import com.coovane.bytetrix.common.exception.GenericException;
import com.coovane.bytetrix.common.exception.ParamIsNullException;
import com.coovane.bytetrix.common.model.system.ResetPasswordDTO;
import com.coovane.bytetrix.common.util.ConvertUtils;
import com.coovane.bytetrix.common.util.Copier;
import com.coovane.bytetrix.common.util.RandomUtils;
import com.coovane.bytetrix.dao.system.AdminUserDao;
import com.coovane.bytetrix.domain.system.AdminUser;
import com.coovane.bytetrix.common.model.system.AdminUserDTO;
import com.coovane.bytetrix.common.model.system.AdminUserListVO;
import com.coovane.bytetrix.common.model.system.AdminUserQuery;
import com.coovane.bytetrix.service.system.AdminUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * <p>
 * 管理员用户service实现
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/14 22:07
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Slf4j
@Service
public class AdminUserServiceImpl extends ServiceImpl<AdminUserDao, AdminUser> implements AdminUserService {

    /**
     * 列表分页查询
     *
     * @param query
     * @return
     */
    @Override
    public IPage<AdminUserListVO> pageQuery(AdminUserQuery query) {
        Page<AdminUser> page = new Page<>(query.getPage(), query.getLimit());
        LambdaQueryWrapper<AdminUser> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(query.getMobile())) {
            queryWrapper.like(AdminUser::getMobile, query.getMobile());
        }
        if (StringUtils.isNotBlank(query.getUsername())) {
            queryWrapper.like(AdminUser::getUsername, query.getUsername());
        }
        if (StringUtils.isNotBlank(query.getNickname())) {
            queryWrapper.like(AdminUser::getNickname, query.getNickname());
        }
        if (Objects.nonNull(query.getEnabled())) {
            queryWrapper.eq(AdminUser::getEnabled, query.getEnabled());
        }
        Page<AdminUser> adminUserPage = this.getBaseMapper().selectPage(page, queryWrapper);
        return adminUserPage.convert(adminUser -> ConvertUtils.convert(adminUser, AdminUserListVO.class));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String create(AdminUserDTO dto) {
        log.info("=====adminUserDTO:{}", JSONObject.toJSONString(dto));
        // TODO
        // 补充一些校验逻辑
        AdminUser adminUser = Copier.copy(dto, new AdminUser());
        adminUser.setSalt(RandomUtils.randomCode(16));
        adminUser.setPassword(RandomUtils.randomCode(24));
        boolean saveResult = this.save(adminUser);
        GenericAssert.isTrue(saveResult, new GenericException(ErrorCodeEnum.OPERATE_FAILED));
        log.info("=====adminUser:{}", JSONObject.toJSONString(adminUser));
        return adminUser.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String modify(AdminUserDTO dto) {
        log.info("=====adminUserDTO:{}", JSONObject.toJSONString(dto));
        // TODO
        // 补充一些校验逻辑
        AdminUser adminUser = Copier.copy(dto, new AdminUser());
        boolean updateResult = this.updateById(adminUser);
        GenericAssert.isTrue(updateResult, new GenericException(ErrorCodeEnum.OPERATE_FAILED));
        log.info("=====adminUser:{}", JSONObject.toJSONString(adminUser));
        return adminUser.getId();
    }

    /**
     * 获取单个详情
     *
     * @param id
     * @return
     */
    @Override
    public AdminUserDTO getDetail(String id) {
        GenericAssert.notNull(id, new ParamIsNullException(ErrorCodeEnum.PARAM_IS_NULL));
        AdminUser adminUser = this.getById(id);
        return ConvertUtils.convert(adminUser, AdminUserDTO.class);
    }

    /**
     * 根据ID逻辑删除
     *
     * @param id
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String deleteById(String id) {
        GenericAssert.notNull(id, new ParamIsNullException(ErrorCodeEnum.PARAM_IS_NULL));
        boolean removeResult = this.removeById(id);
        GenericAssert.isTrue(removeResult, new GenericException(ErrorCodeEnum.OPERATE_FAILED));
        return id;
    }

    /**
     * 重置密码
     * @param dto
     * @return
     */
    @Override
    public String resetPassword(ResetPasswordDTO dto) {
        return null;
    }

}
