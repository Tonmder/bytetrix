package com.coovane.bytetrix.service.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.coovane.bytetrix.common.model.system.AdminPositionDTO;
import com.coovane.bytetrix.common.model.system.AdminPositionListVO;
import com.coovane.bytetrix.common.model.system.AdminPositionQuery;
import com.coovane.bytetrix.domain.system.AdminPosition;

/**
 * <p>
 * 描述：岗位管理操作接口
 * </p >
 *
 * @author: liuchangrong
 * @date: 2022/11/27 21:16
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public interface AdminPositionService extends IService<AdminPosition> {
    /**
     * 分页查询
     * @param query
     * @return
     */
    IPage<AdminPositionListVO> pageQuery(AdminPositionQuery query);

    /**
     * 获取单个详情
     * @param id
     * @return
     */
    AdminPositionDTO getDetail(String id);

    /**
     * 删除
     * @param id
     * @return
     */
    String deleteById(String id);

    /**
     * 新增
     * @param dto
     * @return
     */
    String create(AdminPositionDTO dto);

    /**
     * 编辑更新
     * @param dto
     * @return
     */
    String modify(AdminPositionDTO dto);

    /**
     * 校验是否存在重复数据
     * @param dto
     * @return
     */
    void assertIfDuplicate(AdminPositionDTO dto);
}
