package com.coovane.bytetrix.service.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.coovane.bytetrix.domain.system.AdminRole;
import com.coovane.bytetrix.common.model.system.AdminRoleDTO;
import com.coovane.bytetrix.common.model.system.AdminRoleListVO;
import com.coovane.bytetrix.common.model.system.AdminRoleQuery;

/**
 * <p>
 *     管理员角色service接口
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 23:02
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public interface AdminRoleService extends IService<AdminRole> {

    /**
     * 列表分页查询
     * @param query
     * @return
     */
    IPage<AdminRoleListVO> pageQuery(AdminRoleQuery query);

    /**
     * 新增
     * @param dto
     * @return
     */
    String create(AdminRoleDTO dto);

    /**
     * 编辑更新
     * @param dto
     * @return
     */
    String modify(AdminRoleDTO dto);

    /**
     * 获取单个详情
     * @param id
     * @return
     */
    AdminRoleDTO getDetail(String id);

    /**
     * 根据ID逻辑删除
     * @param id
     * @return
     */
    String deleteById(String id);

    /**
     * 校验是否存在重复数据
     * @param dto
     * @return
     */
    void assertIfDuplicate(AdminRoleDTO dto);

}
