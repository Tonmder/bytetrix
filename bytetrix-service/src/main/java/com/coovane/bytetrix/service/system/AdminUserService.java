package com.coovane.bytetrix.service.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.coovane.bytetrix.common.model.system.ResetPasswordDTO;
import com.coovane.bytetrix.domain.system.AdminUser;
import com.coovane.bytetrix.common.model.system.AdminUserDTO;
import com.coovane.bytetrix.common.model.system.AdminUserListVO;
import com.coovane.bytetrix.common.model.system.AdminUserQuery;

/**
 * <p>
 *     管理员用户service接口
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/14 22:06
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public interface AdminUserService extends IService<AdminUser> {
    /**
     * 分页查询
     * @param query
     * @return
     */
    IPage<AdminUserListVO> pageQuery(AdminUserQuery query);

    /**
     * 新增
     * @param dto
     * @return
     */
    String create(AdminUserDTO dto);
    /**
     * 编辑更新
     * @param dto
     * @return
     */
    String modify(AdminUserDTO dto);

    /**
     * 获取单个详情
     * @param id
     * @return
     */
    AdminUserDTO getDetail(String id);

    /**
     * 根据ID逻辑删除
     * @param id
     * @return
     */
    String deleteById(String id);

    /**
     * 重置密码
     * @param dto
     * @return
     */
    String resetPassword(ResetPasswordDTO dto);
}
