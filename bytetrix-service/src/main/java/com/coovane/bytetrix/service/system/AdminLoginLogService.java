package com.coovane.bytetrix.service.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.coovane.bytetrix.domain.system.AdminLoginLog;
import com.coovane.bytetrix.common.model.system.AdminLoginLogDTO;
import com.coovane.bytetrix.common.model.system.AdminLoginLogListVO;
import com.coovane.bytetrix.common.model.system.AdminLoginLogQuery;

/**
 * <p>
 *     管理员用户登录日志service接口
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 20:17
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public interface AdminLoginLogService extends IService<AdminLoginLog> {

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    IPage<AdminLoginLogListVO> pageQuery(AdminLoginLogQuery query);
    /**
     * 新增
     * @param dto
     * @return
     */
    String create(AdminLoginLogDTO dto);
}
