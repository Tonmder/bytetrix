package com.coovane.bytetrix.service.impl.system;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coovane.bytetrix.common.enumeration.ErrorCodeEnum;
import com.coovane.bytetrix.common.exception.DuplicateException;
import com.coovane.bytetrix.common.exception.GenericAssert;
import com.coovane.bytetrix.common.exception.GenericException;
import com.coovane.bytetrix.common.exception.ParamIsNullException;
import com.coovane.bytetrix.common.util.ConvertUtils;
import com.coovane.bytetrix.common.util.Copier;
import com.coovane.bytetrix.dao.system.AdminResourceDao;
import com.coovane.bytetrix.domain.system.AdminResource;
import com.coovane.bytetrix.common.model.system.AdminResourceDTO;
import com.coovane.bytetrix.common.model.system.AdminResourceListVO;
import com.coovane.bytetrix.common.model.system.AdminResourceQuery;
import com.coovane.bytetrix.service.system.AdminResourceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * <p>
 * 菜单资源管理service实现
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 21:09
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Slf4j
@Service
public class AdminResourceServiceImpl extends ServiceImpl<AdminResourceDao, AdminResource> implements AdminResourceService {

    /**
     * 列表分页查询
     *
     * @param query
     * @return
     */
    @Override
    public IPage<AdminResourceListVO> pageQuery(AdminResourceQuery query) {
        Page<AdminResource> page = new Page<>(query.getPage(), query.getLimit());
        LambdaQueryWrapper<AdminResource> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(query.getResourceName())) {
            queryWrapper.like(AdminResource::getResourceName, query.getResourceName());
        }
        if (Objects.nonNull(query.getEnabled())) {
            queryWrapper.eq(AdminResource::getEnabled, query.getEnabled());
        }
        Page<AdminResource> adminResourcePage = this.getBaseMapper().selectPage(page, queryWrapper);
        return adminResourcePage.convert(adminResource -> ConvertUtils.convert(adminResource, AdminResourceListVO.class));
    }

    /**
     * 新增
     *
     * @param dto
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String create(AdminResourceDTO dto) {
        log.info("=====create adminResourceDTO:{}", JSONObject.toJSONString(dto));
        // 校验是否存在逻辑
        assertIfDuplicate(dto);
        AdminResource adminResource = Copier.copy(dto, new AdminResource());
        boolean saveResult = this.save(adminResource);
        GenericAssert.isTrue(saveResult, new GenericException(ErrorCodeEnum.OPERATE_FAILED));
        log.info("=====create adminResource:{}", JSONObject.toJSONString(adminResource));
        return adminResource.getId();
    }

    /**
     * 编辑更新
     *
     * @param dto
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String modify(AdminResourceDTO dto) {
        log.info("=====modify adminResourceDTO:{}", JSONObject.toJSONString(dto));
        // 校验是否存在逻辑
        assertIfDuplicate(dto);
        AdminResource adminResource = Copier.copy(dto, new AdminResource());
        boolean updateResult = this.updateById(adminResource);
        GenericAssert.isTrue(updateResult, new GenericException(ErrorCodeEnum.OPERATE_FAILED));
        log.info("=====modify adminResource:{}", JSONObject.toJSONString(adminResource));
        return adminResource.getId();
    }

    /**
     * 获取单个详情
     *
     * @param id
     * @return
     */
    @Override
    public AdminResourceDTO getDetail(String id) {
        GenericAssert.notNull(id, new ParamIsNullException(ErrorCodeEnum.PARAM_IS_NULL));
        AdminResource adminResource = this.getById(id);
        return ConvertUtils.convert(adminResource, AdminResourceDTO.class);
    }

    /**
     * 根据ID逻辑删除
     *
     * @param id
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String deleteById(String id) {
        GenericAssert.notNull(id, new ParamIsNullException(ErrorCodeEnum.PARAM_IS_NULL));
        boolean removeResult = this.removeById(id);
        GenericAssert.isTrue(removeResult, new GenericException(ErrorCodeEnum.OPERATE_FAILED));
        return id;
    }

    /**
     * 校验是否存在重复数据
     *
     * @param dto
     * @return
     */
    @Override
    public void assertIfDuplicate(AdminResourceDTO dto) {
        LambdaQueryWrapper<AdminResource> wrapper = new LambdaQueryWrapper<>();
        // 更新的时候需要判断非当前数据
        if (StringUtils.isNotBlank(dto.getId())) {
            wrapper.ne(AdminResource::getId, dto.getId());
        }
        wrapper.eq(AdminResource::getResourceName, dto.getResourceName());
        // 先校验编码是否重复
        boolean exists = this.getBaseMapper().exists(wrapper);
        GenericAssert.isFalse(exists, new DuplicateException(ErrorCodeEnum.PARAM_CODE_DUPLICATE));
    }

}
