package com.coovane.bytetrix.common.model.system;

import lombok.Data;

/**
 * <p>
 *     管理员用户列表VO
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 20:10
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
public class AdminLoginLogListVO {
    /**
     * 主键ID
     */
    private String id;
    /**
     * 管理员用户ID
     */
    private String userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 登录IP
     */
    private String loginIp;
    /**
     * 归属地：广东广州
     */
    private String attribution;
    /**
     * 浏览器类型
     */
    private String browserType;
    /**
     * 平台类型
     */
    private Integer platform;
    /**
     * 登录异常提醒
     */
    private String warning;
}
