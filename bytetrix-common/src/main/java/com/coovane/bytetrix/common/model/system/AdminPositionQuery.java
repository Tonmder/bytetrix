package com.coovane.bytetrix.common.model.system;

import com.coovane.bytetrix.common.request.PageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 描述：岗位关联列表筛选
 * </p >
 *
 * @author: liuchangrong
 * @date: 2022/11/27 21:23
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AdminPositionQuery extends PageRequest {

    /**
     * 岗位编码
     */
    private String positionCode;

    /**
     * 岗位名称
     */
    private String positionName;

    /**
     * 岗位状态
     */
    private Integer enabled;
}
