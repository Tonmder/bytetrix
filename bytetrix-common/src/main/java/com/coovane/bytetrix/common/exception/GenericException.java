package com.coovane.bytetrix.common.exception;

import com.coovane.bytetrix.common.enumeration.ErrorCodeEnum;
import lombok.Data;

/**
 * <p>
 * 通用校验异常
 * 具体业务场景自定义异常需要继承此异常类
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 23:15
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
public class GenericException extends RuntimeException {

    private ErrorCodeEnum errorCodeEnum;
    private String customMessage;

    public GenericException(ErrorCodeEnum errorCodeEnum) {
        super(errorCodeEnum.getErrorMsg());
        this.errorCodeEnum = errorCodeEnum;
    }

    public GenericException(ErrorCodeEnum errorCodeEnum, String customMessage) {
        super(customMessage);
        this.errorCodeEnum = errorCodeEnum;
    }

}
