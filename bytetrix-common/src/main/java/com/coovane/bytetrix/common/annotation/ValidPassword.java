package com.coovane.bytetrix.common.annotation;

import com.coovane.bytetrix.common.validation.PasswordConstraintValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * <p>
 *     密码验证注解
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/8 22:21
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Documented
@Constraint(validatedBy = {PasswordConstraintValidator.class})
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Repeatable(ValidPassword.List.class)
public @interface ValidPassword {

    String message() default "Invalid Password";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        ValidPassword[] value();
    }

}
