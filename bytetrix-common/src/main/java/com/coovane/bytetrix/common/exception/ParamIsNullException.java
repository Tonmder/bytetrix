package com.coovane.bytetrix.common.exception;

import com.coovane.bytetrix.common.enumeration.ErrorCodeEnum;

/**
 * <p>
 * 参数为空异常
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 21:59
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class ParamIsNullException extends GenericException {

    public ParamIsNullException(ErrorCodeEnum errorCodeEnum) {
        super(errorCodeEnum);
    }

    public ParamIsNullException(ErrorCodeEnum errorCodeEnum, String customMessage) {
        super(errorCodeEnum, customMessage);
    }

}
