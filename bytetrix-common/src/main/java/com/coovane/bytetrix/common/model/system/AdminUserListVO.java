package com.coovane.bytetrix.common.model.system;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 *     管理员用户列表VO
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/26 22:04
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
public class AdminUserListVO implements Serializable {
    @Serial
    private static final long serialVersionUID = -4872549070760573553L;
    /**
     * 主键ID
     */
    private String id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 是否启用：0-禁用，1-启用
     */
    private Integer enabled;
}
