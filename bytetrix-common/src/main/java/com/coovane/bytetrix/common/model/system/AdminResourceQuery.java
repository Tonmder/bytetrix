package com.coovane.bytetrix.common.model.system;

import com.coovane.bytetrix.common.request.PageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 菜单资源查询请求类
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 22:52
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AdminResourceQuery extends PageRequest {
    /**
     * 资源名称
     */
    private String resourceName;
    /**
     * 是否启用：0-禁用，1-启用
     */
    private Integer enabled;
}
