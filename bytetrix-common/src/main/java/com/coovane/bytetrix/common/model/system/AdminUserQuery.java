package com.coovane.bytetrix.common.model.system;

import com.coovane.bytetrix.common.request.PageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *     管理员用户查询请求类
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/26 23:11
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AdminUserQuery extends PageRequest {
    /**
     * 用户名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 是否启用：0-禁用，1-启用
     */
    private Integer enabled;
}
