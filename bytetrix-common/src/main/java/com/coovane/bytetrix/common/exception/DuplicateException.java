package com.coovane.bytetrix.common.exception;

import com.coovane.bytetrix.common.enumeration.ErrorCodeEnum;

/**
 * <p>
 * 数据重复异常
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 22:39
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class DuplicateException extends GenericException {

    public DuplicateException(ErrorCodeEnum errorCodeEnum) {
        super(errorCodeEnum);
    }

    public DuplicateException(ErrorCodeEnum errorCodeEnum, String customMessage) {
        super(errorCodeEnum, customMessage);
    }

}
