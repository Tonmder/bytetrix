package com.coovane.bytetrix.common.util;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * <p>
 * 常用正则工具类
 * </p>
 *
 * @author: Hsu
 * @date: 2021/09/28 22:59
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class RegexUtils {
    /**
     * 中国移动电话（手机号）正则
     */
    public static final String REGEX_MOBILE = "^1[3-9]\\d{9}$";
    /**
     * 中国移动手机号正则
     * 134(0-8)、135、136、137、138、139、144(物)、147、148(物)、150、151、152、157、158、159、165(虚)、1703(虚)、1705(虚)、1706(虚)、172(物)、178
     * 182、183、184、187、188、195、197(5G)、198
     */
    public static final String REGEX_CHINA_MOBILE = "^1(34[0-8]|(3[5-9]|4[478]|5[0127-9]|65|7[28]|8[2-478]|9[578])[0-9]|703|70[56])\\d{7}$";
    /**
     * 中国联通手机号正则
     * 130、131、132、140(物)、145、146(物)、155、156、166、167(虚)、1704(虚)、1707(虚)、1708(虚)、1709(虚)，171(虚)、175、176、185、186、196(5G)
     */
    public static final String REGEX_CHINA_UNICOM = "^1((3[0-2]|4[056]|5[56]|6[67]|7[156]|8[56]|96)[0-9]|170[47-9])\\d{7}$";
    /**
     * 中国电信手机号正则
     * 133、1349(卫)、141(物)、149(物)、153、162(虚)、1700(虚)、1701(虚)、1702(虚)、173、1740(0-5)(卫)、177、180、181、189、190(5G)、191、193、199
     */
    public static final String REGEX_CHINA_TELECOM = "^1((33|4[19]|53|62|7[37]|8[019]|9[0139])[0-9][0-9]|700[0-2]|349[0-9]|740[0-5])\\d{6}$";
    /**
     * 中国广电手机号正则
     * 192(5G)
     */
    public static final String REGEX_CHINA_BROAD_NET = "^192\\d{8}$";
    /**
     * 中国固定电话正则及小灵通
     * 区号：010,020,021,022,023,024,025,027,028,029
     * 号码：七位或八位
     */
    public static final String REGEX_TELEPHONE = "^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    /**
     * 电子邮箱正则
     * 名称允许汉字、字母、数字，域名只允许英文域名
     */
    public static final String REGEX_EMAIL = "^[A-Za-z0-9\\u4e00-\\u9fa5]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";

    public static void main(String[] args) {
        System.out.println("=====" + isMobile("18218779941"));
        System.out.println("=====" + mobileServiceProvider("18318779941"));
        System.out.println("=====" + mobileServiceProvider("13247585315"));
        System.out.println("=====" + mobileServiceProvider("19977511207"));
        System.out.println("=====" + mobileServiceProvider("19277511207"));
        System.out.println("=====" + isTelephone("0205646580"));
        System.out.println("=====" + isEmail("0205646580@163.com"));
    }

    /**
     * 判断是否是大陆手机号
     *
     * @param inputStr
     * @return
     */
    public static boolean isMobile(String inputStr) {
        if (Objects.nonNull(inputStr)) {
            return Pattern.matches(REGEX_MOBILE, inputStr);
        }
        return false;
    }

    /**
     * 判断是否是中国移动手机号
     *
     * @param inputStr
     * @return
     */
    public static boolean isChinaMobile(String inputStr) {
        if (Objects.nonNull(inputStr)) {
            return Pattern.matches(REGEX_CHINA_MOBILE, inputStr);
        }
        return false;
    }

    /**
     * 判断是否是中国联通手机号
     *
     * @param inputStr
     * @return
     */
    public static boolean isChinaUnicom(String inputStr) {
        if (Objects.nonNull(inputStr)) {
            return Pattern.matches(REGEX_CHINA_UNICOM, inputStr);
        }
        return false;
    }

    /**
     * 判断是否是中国电信手机号
     *
     * @param inputStr
     * @return
     */
    public static boolean isChinaTelecom(String inputStr) {
        if (Objects.nonNull(inputStr)) {
            return Pattern.matches(REGEX_CHINA_TELECOM, inputStr);
        }
        return false;
    }

    /**
     * 判断是否是中国广电手机号
     *
     * @param inputStr
     * @return
     */
    public static boolean isChinaBroadNet(String inputStr) {
        if (Objects.nonNull(inputStr)) {
            return Pattern.matches(REGEX_CHINA_BROAD_NET, inputStr);
        }
        return false;
    }

    /**
     * 判断是否是固定电话及小灵通
     *
     * @param inputStr
     * @return
     */
    public static boolean isTelephone(String inputStr) {
        if (Objects.nonNull(inputStr)) {
            return Pattern.matches(REGEX_TELEPHONE, inputStr);
        }
        return false;
    }

    /**
     * 判断手机号运营商
     *
     * @param inputStr
     * @return
     */
    public static String mobileServiceProvider(String inputStr) {
        if (isChinaMobile(inputStr)) {
            return "中国移动";
        }
        if (isChinaUnicom(inputStr)) {
            return "中国联通";
        }
        if (isChinaTelecom(inputStr)) {
            return "中国电信";
        }
        if (isChinaBroadNet(inputStr)) {
            return "中国广电";
        }
        return "未知";
    }

    /**
     * 判断是否是电子邮箱
     *
     * @param inputStr
     * @return
     */
    public static boolean isEmail(String inputStr) {
        if (Objects.nonNull(inputStr)) {
            return Pattern.matches(REGEX_EMAIL, inputStr);
        }
        return false;
    }

}
