package com.coovane.bytetrix.common.model.system;

import com.coovane.bytetrix.common.annotation.ConfirmPassword;
import com.coovane.bytetrix.common.annotation.ValidPassword;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 重置密码DTO
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/9 22:36
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
@ConfirmPassword(message = "两次密码不一致")
public class ResetPasswordDTO implements Serializable {
    /**
     * 新密码
     */
    @ValidPassword
    private String newPassword;
    /**
     * 确认密码
     */
    private String confirmPassword;
}
