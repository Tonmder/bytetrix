package com.coovane.bytetrix.common.enumeration;

import java.util.Objects;

/**
 * <p>
 *     数据权限范围枚举
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 22:48
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public enum DataScopeEnum {
    ALL(1, "全部数据"),
    THIS_DEPARTMENT(2, "本部门数据"),
    THIS_DEPARTMENT_AND_CHILDREN(3, "本部门及以下数据"),
    ONLY_SELF(4, "仅本人数据"),
    CUSTOM(5, "自定数据");

    private Integer code;
    private String description;

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    DataScopeEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public static DataScopeEnum getByCode(Integer code) {
        DataScopeEnum[] values = values();
        int length = values.length;

        for (int i = 0; i < length; ++i) {
            DataScopeEnum anEnum = values[i];
            if (Objects.equals(anEnum.getCode(), code)) {
                return anEnum;
            }
        }
        return DataScopeEnum.ONLY_SELF;
    }
}
