package com.coovane.bytetrix.common.util;

import jakarta.validation.constraints.NotNull;

import java.util.UUID;

/**
 * <p>
 * UUID相关工具类
 * </p>
 *
 * @author: Hsu
 * @date: 2021/7/7 22:06
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class UUIDUtils {

    /**
     * 默认UUID随机生成方法
     */
    public static String getUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * UUID随机生成并替换字符方法
     *
     * @param target      需要替换的字符
     * @param replacement 替换字符
     * @return
     */
    public static String getUUID(@NotNull CharSequence target, @NotNull CharSequence replacement) {
        return getUUID().replace(target, replacement);
    }

    /**
     * UUID随机生成并替换字符方法
     *
     * @param target      需要替换的字符
     * @param replacement 替换字符
     * @return
     */
    public static String getLowerUUID(@NotNull CharSequence target, @NotNull CharSequence replacement) {
        return getUUID(target, replacement).toLowerCase();
    }

}
