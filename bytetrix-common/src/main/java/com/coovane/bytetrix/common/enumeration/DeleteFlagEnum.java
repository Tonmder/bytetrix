package com.coovane.bytetrix.common.enumeration;

import java.util.Objects;

/**
 * <p>
 * 删除状态枚举
 * </p>
 *
 * @author: Hsu
 * @date: 2022/5/23 23:00
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public enum DeleteFlagEnum {
    NOT_DELETE(0, "未删除"),
    DELETED(1, "已删除");

    private Integer code;
    private String description;

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    DeleteFlagEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public static DeleteFlagEnum getByCode(Integer code) {
        DeleteFlagEnum[] values = values();
        int length = values.length;

        for (int i = 0; i < length; ++i) {
            DeleteFlagEnum anEnum = values[i];
            if (Objects.equals(anEnum.getCode(), code)) {
                return anEnum;
            }
        }
        return DeleteFlagEnum.NOT_DELETE;
    }

}
