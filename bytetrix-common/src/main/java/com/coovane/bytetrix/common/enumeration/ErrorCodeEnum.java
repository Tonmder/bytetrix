package com.coovane.bytetrix.common.enumeration;

import lombok.Getter;

import java.util.Locale;
import java.util.Objects;

/**
 * <p>
 * 错误码枚举类
 * </p>
 *
 * @author: Hsu
 * @date: 2022/5/23 19:13
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Getter
public enum ErrorCodeEnum {
    UNKNOWN_INTERNAL_ERROR("unknown_internal_error", "-1", "未知错误，请联系管理员"),
    ILLEGAL_ARGUMENT("illegal_argument", "100001", "参数非法"),
    SIZE_LIMIT_EXCEEDED("size_limit_exceeded", "100002", "超出大小限制"),
    FIELD_BIND_ERROR("field_bind_error", "100003", "参数校验错误"),
    LINK_FAILURE("link_failure","100004","连接失败"),
    PARAM_IS_NULL("param_is_null", "100005", "参数为空"),
    PARAM_CODE_DUPLICATE("param_code_duplicate", "100006", "编码重复"),
    PARAM_NAME_DUPLICATE("param_name_duplicate", "100007", "名称重复"),
    HISTORY_VIOLATION("history_violation", "100008", "密码和您最近用过的%1$s个密码之一重复"),
    ILLEGAL_WORD("illegal_word", "100009", "密码包含了黑名单字典中的词%1$s"),
    ILLEGAL_WORD_REVERSED("illegal_word_reversed", "100010", "密码包含了保留字典中的词%1$s"),
    ILLEGAL_DIGEST_WORD("illegal_digest_word", "100011", "密码包含了字典中的词"),
    ILLEGAL_DIGEST_WORD_REVERSED("illegal_digest_word_reversed", "100012", "密码包含了保留字典中的词"),
    ILLEGAL_MATCH("illegal_match", "100013", "密码匹配了非法结构%1$s"),
    ALLOWED_MATCH("allowed_match", "100014", "密码必须要匹配结构%1$s"),
    ILLEGAL_CHAR("illegal_char", "100015", "密码%2$s非法字符%1$s"),
    ALLOWED_CHAR("allowed_char", "100016", "密码%2$s非法字符%1$s"),
    ILLEGAL_QWERTY_SEQUENCE("illegal_qwerty_sequence", "100017", "密码包含非法的QWERTY序列%1$s"),
    ILLEGAL_ALPHABETICAL_SEQUENCE("illegal_alphabetical_sequence", "100018", "密码包含非法的字母序列%1$s"),
    ILLEGAL_NUMERICAL_SEQUENCE("illegal_numerical_sequence", "100019", "密码包含非法的数字序列%1$s"),
    ILLEGAL_USERNAME("illegal_username", "100020", "密码%2$s用户id %1$s"),
    ILLEGAL_USERNAME_REVERSED("illegal_username_reversed", "100021", "密码%2$s倒序的用户 id %1$s"),
    ILLEGAL_WHITESPACE("illegal_whitespace", "100022", "密码包含空格"),
    ILLEGAL_NUMBER_RANGE("illegal_number_range", "100023", "密码%2$s数字%1$s"),
    ILLEGAL_REPEATED_CHARS("illegal_repeated_chars", "100024", "密码中包含%2$s序列%1$s的一个或多个重复字符, 但仅允许%2$s个: %4$s"),
    INSUFFICIENT_UPPERCASE("insufficient_uppercase", "100025", "密码中必须包含至少%1$s个大写字母"),
    INSUFFICIENT_LOWERCASE("insufficient_lowercase", "100026", "密码中必须包含至少%1$s个小写字母"),
    INSUFFICIENT_ALPHABETICAL("insufficient_alphabetical", "100027", "密码中必须包含至少%1$s个字母"),
    INSUFFICIENT_DIGIT("insufficient_digit", "100028", "密码中必须包含至少%1$s个数字"),
    INSUFFICIENT_SPECIAL("insufficient_special", "100029", "密码中必须包含至少%1$s个特殊字符"),
    INSUFFICIENT_CHARACTERISTICS("insufficient_characteristics", "100030", "密码匹配了%1$s of %3$s字符规则, 但只允许%2$s个"),
    INSUFFICIENT_COMPLEXITY("insufficient_complexity", "100031", "密码符合了%2$s个复杂规则, 但需要符合%3$s个"),
    INSUFFICIENT_COMPLEXITY_RULES("insufficient_complexity_rules", "100032", "对于密码长度%1$s，没有配置规则"),
    SOURCE_VIOLATION("source_violation", "100033", "密码不能和之前的%1$s个历史密码相同"),
    TOO_LONG("too_long", "100034", "密码长度不能超过%2$s个字符"),
    TOO_SHORT("too_short", "100035", "密码长度不能少于%1$s个字符"),
    TOO_MANY_OCCURRENCES("too_many_occurrences", "100036", "密码包含%2$s个%1$s, 但是至多只允许%2$s个"),
    OPERATE_FAILED("operate_failed", "999999", "操作失败");

    // 国际化代码
    private String localeCode;
    private String errorCode;
    private String errorMsg;

    ErrorCodeEnum(String localeCode, String errorCode, String errorMsg) {
        this.localeCode = localeCode;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public static ErrorCodeEnum resolve(String errorCode) {
        Locale china = Locale.CHINA;
        ErrorCodeEnum[] values = values();
        int length = values.length;

        for (int i = 0; i < length; ++i) {
            ErrorCodeEnum anEnum = values[i];
            if (Objects.equals(anEnum.getErrorCode(), errorCode)) {
                return anEnum;
            }
        }

        return ErrorCodeEnum.UNKNOWN_INTERNAL_ERROR;
    }

    public static ErrorCodeEnum resolveLocale(String localeCode) {
        ErrorCodeEnum[] values = values();
        int length = values.length;

        for (int i = 0; i < length; ++i) {
            ErrorCodeEnum anEnum = values[i];
            if (Objects.equals(anEnum.getLocaleCode(), localeCode)) {
                return anEnum;
            }
        }

        return ErrorCodeEnum.UNKNOWN_INTERNAL_ERROR;
    }

}
