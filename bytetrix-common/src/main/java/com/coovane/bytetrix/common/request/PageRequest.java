package com.coovane.bytetrix.common.request;

import java.io.Serializable;

/**
 * <p>
 *     分页查询参数
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/8 22:57
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class PageRequest implements Serializable {

    private static final long serialVersionUID = -5940532279209703055L;
    private int page = 1;
    private int limit = 10;

    public PageRequest() {
    }

    public PageRequest(int page, int limit) {
        this.page = page;
        this.limit = limit;
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return this.limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        int offset = (this.page - 1) * this.limit;
        return offset < 0 ? 0 : offset;
    }

}
