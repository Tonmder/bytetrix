package com.coovane.bytetrix.common.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 通用分页响应类
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/8 22:57
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class PageResponse<T> implements Serializable {

    private static final long serialVersionUID = 3848582677260594107L;
    private long totalCount = 0L;
    private long totalPage = 1L;
    private long pageSize = 10L;
    private long currentPage = 1L;
    private List<T> list = new ArrayList<>();

    public PageResponse() {
    }

    public PageResponse(long totalCount, long pageSize, long currentPage, List<T> list) {
        this.totalCount = totalCount;
        this.totalPage = totalCount / pageSize + 1;
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.list = list;
    }

    public static <T> PageResponse<T> success(List<T> list, long currentPage, long pageSize, long totalCount) {
        return list == null ? empty(currentPage, pageSize) : new PageResponse<>(totalCount, pageSize, currentPage, list);
    }

    public static <T> PageResponse<T> empty(long currentPage, long pageSize) {
        return new PageResponse<>(0L, pageSize, currentPage, Collections.emptyList());
    }

    public <U> PageResponse<U> map(Function<? super T, ? extends U> converter) {
        return PageResponse.success(this.list.stream().map(converter).collect(Collectors.toList()), this.currentPage, this.pageSize, this.totalCount);
    }

    public PageResponse<T> sorted(Comparator<? super T> comparator) {
        return PageResponse.success(this.list.stream().sorted(comparator).collect(Collectors.toList()), this.currentPage, this.pageSize, this.totalCount);
    }

    public PageResponse<T> sorted() {
        return PageResponse.success(this.list.stream().sorted().collect(Collectors.toList()), this.currentPage, this.pageSize, this.totalCount);
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getTotalPage() {
        long total = this.totalCount;
        if (total < this.pageSize) {
            return this.totalPage;
        }
        return total % this.pageSize == 0 ? total / this.pageSize : total / this.pageSize + 1;
    }

    public void setTotalPage(long totalPage) {
        this.totalPage = totalPage;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(long currentPage) {
        this.currentPage = currentPage;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

}
