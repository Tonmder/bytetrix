package com.coovane.bytetrix.common.model.system;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 * 描述：岗位管理操作DTO
 * </p >
 *
 * @author: liuchangrong
 * @date: 2022/11/27 21:10
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
public class AdminPositionDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = -6963524976653518981L;

    /**
     * 主键
     */
    private String id;

    /**
     * 岗位编码
     */
    private String positionCode;

    /**
     * 岗位名称
     */
    private String positionName;

    /**
     * 显示顺序
     */
    private Integer sortNum;

    /**
     * 岗位状态
     */
    private Integer enabled;
}
