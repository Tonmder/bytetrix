package com.coovane.bytetrix.common.annotation;

import com.coovane.bytetrix.common.validation.MobileConstraintValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * <p>
 * 手机号验证注解
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/8 21:32
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Documented
@Constraint(validatedBy = {MobileConstraintValidator.class})
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Repeatable(ValidMobile.List.class)
public @interface ValidMobile {

    String message() default "Invalid mobile";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        ValidMobile[] value();
    }

}
