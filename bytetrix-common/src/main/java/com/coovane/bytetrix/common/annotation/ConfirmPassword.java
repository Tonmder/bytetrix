package com.coovane.bytetrix.common.annotation;

import com.coovane.bytetrix.common.validation.ConfirmPasswordConstraintValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

/**
 * <p>
 * 确认密码验证注解
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/11 20:38
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Documented
@Constraint(validatedBy = {ConfirmPasswordConstraintValidator.class})
@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE})
public @interface ConfirmPassword {

    String message() default "Password not equals";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
