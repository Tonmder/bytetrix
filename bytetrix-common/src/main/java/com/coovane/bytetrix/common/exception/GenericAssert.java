package com.coovane.bytetrix.common.exception;

import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Objects;

/**
 * <p>
 *     自定义通用断言
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 23:07
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class GenericAssert {

    /**
     * 判断是否为true，不为true时抛出异常
     * @param expression
     * @param exception
     */
    public static void isTrue(boolean expression, GenericException exception) {
        if (!expression) {
            throw exception;
        }
    }

    /**
     * 判断是否为false，不为false时抛出异常
     * @param expression
     * @param exception
     */
    public static void isFalse(boolean expression, GenericException exception) {
        if (expression) {
            throw exception;
        }
    }

    /**
     * 判断是否为null，为null时抛出异常
     * @param object
     * @param exception
     */
    public static void notNull(@Nullable Object object, GenericException exception) {
        if (Objects.isNull(object)) {
            throw exception;
        }
    }

    /**
     * 判断是否为空集合，为空集合时抛出异常
     * @param collection
     * @param exception
     */
    public static void notEmpty(@Nullable Collection<?> collection, GenericException exception) {
        if (CollectionUtils.isEmpty(collection)) {
            throw exception;
        }
    }

}
