package com.coovane.bytetrix.common.validation;

import com.coovane.bytetrix.common.annotation.ConfirmPassword;
import com.coovane.bytetrix.common.model.system.ResetPasswordDTO;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Objects;

/**
 * <p>
 *     确认密码验证器
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/11 20:39
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class ConfirmPasswordConstraintValidator implements ConstraintValidator<ConfirmPassword, ResetPasswordDTO> {

    @Override
    public void initialize(ConfirmPassword constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(ResetPasswordDTO dto, ConstraintValidatorContext context) {
        return Objects.equals(dto.getNewPassword(), dto.getConfirmPassword());
    }

}
