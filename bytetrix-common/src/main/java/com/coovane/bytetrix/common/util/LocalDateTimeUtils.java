package com.coovane.bytetrix.common.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;

/**
 * <p>
 * JDK1.8版本以上时间工具类
 * </p>
 *
 * @author: Hsu
 * @date: 2022/7/22 21:45
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class LocalDateTimeUtils {

    /**
     * LocalDateTime to Date
     *
     * @param localDateTime
     * @return
     */
    public static Date localDateTime2Date(LocalDateTime localDateTime) {
        if (Objects.nonNull(localDateTime)) {
            Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
            return Date.from(instant);
        }
        return null;
    }

    /**
     * 将时分秒格式的时间转换为数组00:12:32
     *
     * @param duration
     * @return
     */
    public static Long duration2Number(String duration) {
        Long result = 0L;
        if (Objects.isNull(duration) || duration.trim().length() == 0) {
            return result;
        }
        String[] durationArr = duration.split(":");
        if (durationArr.length != 3) {
            return result;
        }
        for (int i = 0; i < durationArr.length; i++) {
            Long timeInt = Long.valueOf(durationArr[i]);
            if (i == 0) {
                if (timeInt > 23) {
                    return result;
                } else {
                    result += timeInt * 60;
                }
            }
            if (i == 1) {
                if (timeInt > 59) {
                    return result;
                } else {
                    result += timeInt * 60;
                }
            }
            if (i == 2) {
                result += timeInt;
            }
        }
        return result;
    }

    /**
     * 将秒数值转换为时分秒格式00:14:43
     *
     * @param secondNum
     * @return
     */
    public static String second2Duration(Integer secondNum) {
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (secondNum <= 0) {
            return "00:00:00";
        } else {
            if (secondNum >= 3600) {
                hour = secondNum / 3600;
                secondNum = secondNum % 3600;
            }
            if (secondNum >= 60) {
                minute = secondNum / 60;
                secondNum = secondNum % 60;
            }
            if (secondNum > 0 && secondNum < 60) {
                second = secondNum;
            }
            return timeFormat(hour) + ":" + timeFormat(minute) + ":" + timeFormat(second);
        }
    }

    public static String timeFormat(int num) {
        String retStr = null;
        if (num >= 0 && num < 10) {
            retStr = "0" + Integer.toString(num);
        } else {
            retStr = "" + num;
        }
        return retStr;
    }

    public static String formatHour(Integer hour) {
        if (hour <= 0) {
            return "00:00";
        }
        String hourStr = "";
        if (hour < 10) {
            hourStr = "0" + hour;
        } else {
            hourStr = hour.toString();
        }
        return hourStr + ":00";
    }

}
