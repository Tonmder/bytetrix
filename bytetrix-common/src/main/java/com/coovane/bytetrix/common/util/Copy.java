package com.coovane.bytetrix.common.util;

/**
 * <p>
 * 把source转换为target需要的包装器类型或者原始类型
 * </p>
 *
 * @author: Hsu
 * @date: 2021/7/2 23:11
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public interface Copy {

    void copy(Object source, Object target, String ignoreProperties);

    void merge(Object source, Object target, String ignoreProperties);

}