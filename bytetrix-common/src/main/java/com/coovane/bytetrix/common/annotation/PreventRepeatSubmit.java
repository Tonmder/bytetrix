package com.coovane.bytetrix.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * <p>
 * 防止重复提交注解
 * </p>
 *
 * @author: Hsu
 * @date: 2022/5/23 21:12
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
public @interface PreventRepeatSubmit {

    /**
     * 持有锁的时间
     * expiredTime时间到自动释放 防止假死 死锁
     * 过期时间(单位秒)
     */
    long expiredTime() default 30L;

    /**
     * 等待时间
     * 尝试等待锁的时间 等不到就返回false
     * (单位秒)
     *
     * @return long
     */
    long waitTime() default 1L;


    /**
     * key前缀
     *
     * @return {@link String}
     */
    String keyPrefix();

    /**
     * 指定组成分布式锁的key，以逗号分隔。
     * 如：keyParts="name,id" 会分拆连接 前缀会拼接keyPrefix
     * 例如： keyPrefix+name+id
     */
    String keyParts();

    /**
     * 参数位置,从0开始
     *
     * @return
     */
    int paramLocation() default 0;

}
