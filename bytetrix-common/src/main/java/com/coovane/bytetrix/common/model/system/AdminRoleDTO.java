package com.coovane.bytetrix.common.model.system;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 *     管理员角色DTO
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 22:40
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
public class AdminRoleDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = 6578183901790882108L;
    /**
     * 主键ID
     */
    private String id;
    /**
     * 角色代码
     */
    @NotBlank(message = "角色编码必填")
    private String roleCode;
    /**
     * 角色名称
     */
    @NotBlank(message = "角色名称必填")
    private String roleName;
    /**
     * 数据范围：1-全部数据，2-本部门数据，3-本部门及以下数据，4-仅本人数据，5-自定数据
     */
    private Integer dataScope;
    /**
     * 是否启用：0-禁用，1-启用
     */
    private Integer enabled;
}
