package com.coovane.bytetrix.common.model.system;

import com.coovane.bytetrix.common.request.PageRequest;
import lombok.Data;

/**
 * <p>
 *     管理员用户查询请求类
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 20:11
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
public class AdminLoginLogQuery extends PageRequest {
    /**
     * 管理员用户ID
     */
    private String userId;
    /**
     * 用户名
     */
    private String username;
}
