package com.coovane.bytetrix.common.model.system;

import com.coovane.bytetrix.common.request.PageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *     管理员角色查询请求类
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 22:45
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AdminRoleQuery extends PageRequest {
    /**
     * 角色代码
     */
    private String roleCode;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 数据范围：1-全部数据，2-本部门数据，3-本部门及以下数据，4-仅本人数据，5-自定数据
     */
    private Integer dataScope;
    /**
     * 是否启用：0-禁用，1-启用
     */
    private Integer enabled;
}
