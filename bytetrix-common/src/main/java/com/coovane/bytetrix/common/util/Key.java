package com.coovane.bytetrix.common.util;

/**
 * <p>
 * 把source转换为target需要的包装器类型或者原始类型
 * </p>
 *
 * @author: Hsu
 * @date: 2021/7/2 23:11
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class Key {
    private Class<?> fromClass;
    private Class<?> toClass;

    public Key(Class<?> fromClass, Class<?> toClass) {
        this.fromClass = fromClass;
        this.toClass = toClass;
    }

    public Key(Class<?> fromClass, Class<?> toClass, String[] ignoreProperties) {
        super();
        this.fromClass = fromClass;
        this.toClass = toClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Key key = (Key) o;
        return fromClass.equals(key.fromClass)
                && toClass.equals(key.toClass);
    }

    @Override
    public int hashCode() {
        int h = fromClass.hashCode();
        h = 31 * h + toClass.hashCode();
        return h;
    }
}