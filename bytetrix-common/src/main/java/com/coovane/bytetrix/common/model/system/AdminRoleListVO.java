package com.coovane.bytetrix.common.model.system;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 *     管理员角色列表VO
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 22:42
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
public class AdminRoleListVO implements Serializable {
    @Serial
    private static final long serialVersionUID = 3238200346548215744L;
    /**
     * 主键ID
     */
    private String id;
    /**
     * 角色代码
     */
    private String roleCode;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 数据范围：1-全部数据，2-本部门数据，3-本部门及以下数据，4-仅本人数据，5-自定数据
     */
    private String dataScopeName;
    /**
     * 是否启用：0-禁用，1-启用
     */
    private Integer enabled;
}
