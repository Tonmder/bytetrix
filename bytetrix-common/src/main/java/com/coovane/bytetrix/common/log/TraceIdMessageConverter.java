package com.coovane.bytetrix.common.log;

import ch.qos.logback.classic.pattern.MessageConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.coovane.bytetrix.common.util.TraceLogUtils;

/**
 * <p>
 *     TraceId消息转换类
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/9 22:28
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class TraceIdMessageConverter extends MessageConverter {

    public TraceIdMessageConverter() {
    }

    public String convert(ILoggingEvent event) {
        return TraceLogUtils.getTraceId("");
    }

}
