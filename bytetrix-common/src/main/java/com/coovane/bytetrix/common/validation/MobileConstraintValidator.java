package com.coovane.bytetrix.common.validation;

import com.coovane.bytetrix.common.annotation.ValidMobile;
import com.coovane.bytetrix.common.util.RegexUtils;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * <p>
 * 自定义手机号验证器
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/8 21:34
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public class MobileConstraintValidator implements ConstraintValidator<ValidMobile, String> {

    @Override
    public void initialize(ValidMobile constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (Objects.nonNull(value) && value.trim().length() > 0) {
            return Pattern.matches(RegexUtils.REGEX_MOBILE, value);
        }
        // 默认可以不填写手机号，即不校验
        return true;
    }

}
