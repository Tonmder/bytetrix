package com.coovane.bytetrix.common.enumeration;

import java.util.Objects;

/**
 * <p>
 *     是否启用枚举
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/30 22:54
 * @copyright: companyName
 * @see
 * @since 1.8
 */
public enum EnabledEnum {
    FORBIDDEN(0, "禁用"),
    ENABLED(1, "启用");

    private Integer code;
    private String description;

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    EnabledEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public static EnabledEnum getByCode(Integer code) {
        EnabledEnum[] values = values();
        int length = values.length;

        for (int i = 0; i < length; ++i) {
            EnabledEnum anEnum = values[i];
            if (Objects.equals(anEnum.getCode(), code)) {
                return anEnum;
            }
        }
        return EnabledEnum.FORBIDDEN;
    }

}
