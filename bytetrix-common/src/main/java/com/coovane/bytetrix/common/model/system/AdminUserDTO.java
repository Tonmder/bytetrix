package com.coovane.bytetrix.common.model.system;

import com.coovane.bytetrix.common.annotation.ValidMobile;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 *     管理员用户DTO
 * </p>
 *
 * @author: Hsu
 * @date: 2022/11/26 21:03
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
public class AdminUserDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = -6963524976653518988L;
    /**
     * 主键ID
     */
    private String id;
    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空")
    private String username;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 手机号
     */
    @ValidMobile(message = "手机号格式不正确")
    private String mobile;
    /**
     * 电子邮箱
     */
    @Email
    private String email;
    /**
     * 性别，2-保密，1-男，0-女
     */
    private Integer gender;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 是否启用：0-禁用，1-启用
     */
    private Integer enabled;
    /**
     * 部门ID
     */
    private String departmentId;
    /**
     * 岗位ID
     */
    private String positionId;
}
