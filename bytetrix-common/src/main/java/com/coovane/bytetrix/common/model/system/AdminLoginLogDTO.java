package com.coovane.bytetrix.common.model.system;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 *     管理员用户登录日志DTO
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 20:07
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
public class AdminLoginLogDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = -8677812473181162586L;
    /**
     * 主键ID
     */
    private String id;
    /**
     * 管理员用户ID
     */
    private String userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 登录IP
     */
    private String loginIp;
    /**
     * 归属地：广东广州
     */
    private String attribution;
    /**
     * 浏览器类型
     */
    private String browserType;
    /**
     * 平台类型
     */
    private Integer platform;
    /**
     * 登录异常提醒
     */
    private String warning;
}
