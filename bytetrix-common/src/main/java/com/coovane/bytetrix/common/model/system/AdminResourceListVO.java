package com.coovane.bytetrix.common.model.system;

import lombok.Data;

/**
 * <p>
 *     菜单资源列表VO
 * </p>
 *
 * @author: Hsu
 * @date: 2022/12/2 23:36
 * @copyright: companyName
 * @see
 * @since 1.8
 */
@Data
public class AdminResourceListVO {
    /**
     * 主键ID
     */
    private String id;
    /**
     * 上级资源ID
     */
    private String parentId;
    /**
     * 资源名称
     */
    private String resourceName;
    /**
     * 资源类型：0-目录，1-菜单，2-按钮
     */
    private Integer resourceType;
    /**
     * 路由地址
     */
    private String routePath;
    /**
     * 访问URL
     */
    private String resourceUrl;
    /**
     * 资源图标
     */
    private String displayIcon;
    /**
     * 显示顺序(从小到大升序)
     */
    private Integer sortNum;
    /**
     * 是否启用：0-禁用，1-启用
     */
    private Integer enabled;
}
